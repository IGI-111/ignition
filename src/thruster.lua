#!/usr/bin/env lua

local rocket = require "rocket"

-- Paths are relative to the project root
rocket.binPath = "bin"     -- Compiled files will be stored here
rocket.srcPath = "src"     -- Source files will be stored here
rocket.objPath = "obj" -- Object files will stored here


function liftoff(name, customPayload, language, compiler, hasMakefile, vcs)
	print("Liftoff!")

	-- making the root directory of the project
	lfs.mkdir(name)
	lfs.chdir(name)

	-- making the different project directories
	-- TODO: make this optionnal based on the libraries used and the language
	lfs.mkdir(rocket.srcPath)
	lfs.mkdir(rocket.objPath)
	lfs.mkdir(rocket.binPath)

	-- adding the requested payload
	rocket.deliverPayload(customPayload)
	-- adding the basic source files and a makefile if possible and requested
	rocket.deliverPayload(language)
	if hasMakefile then rocket.deliverMakefile(name, language, compiler) end
	-- starting a repository for the version control system if asked
	rocket.enableVersionControl(vcs)

	lfs.chdir("..")
end

print("Heating Thruster...")

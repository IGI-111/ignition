#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <getopt.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "main.h"


int main(int argc, char *argv[])
{
	char *language = NULL;
	char *customPayload = NULL;
	char *compiler = NULL;
	char *vcs = NULL;
	bool hasMakefile = false;

	int choice;
	while (1)
	{
		static struct option long_options[] =
		{
			{"help",    no_argument,    0,  'h'},
			{"language", required_argument, 0, 'l'},
			{"makefile", no_argument,       0, 'm'},
			{"compiler", required_argument, 0, 'c'},
			{"version control", required_argument, 0, 'v'},
			{"payload", required_argument, 0, 'p'},
			{0, 0, 0, 0}
		};

		int option_index = 0;

		choice = getopt_long( argc, argv, "hl:mc:v:p:",
				long_options, &option_index);

		if (choice == -1)
			break;

		switch ( choice )
		{
			case 'h':
				printf(
						"Usage: ignition [options] names...\n"
						"Options:\n"
						"\t-l <language>       --language        The project will use the specified language (see documentation for available options)\n"
						"\t-m                  --makefile        The project will have a GNU make Makefile\n"
						"\t-c <compiler>       --compiler        The project will use the specified compiler (to be used with -m)\n"
						"\t-v <vcs>            --version-control The project will use the specified version control system\n"
						"\t-p <custom payload> --payload         The project base will be the specified custom payload; overrides arguments -l and -m\n"
						"\t-h                  --help            Displays this text\n");
				return EXIT_SUCCESS;
			case 'm':
				hasMakefile = true;
				break;
			case 'l':
				language = optarg;
				break;
			case 'p':
				customPayload = optarg;
				break;
			case 'v':
				vcs = optarg;
				break;
			case 'c':
				compiler = optarg;
				break;
			case '?':
				/* getopt_long will have already printed an error */
				break;

			default:
				/* Not sure how to get here... */
				return EXIT_FAILURE;
		}
	}

	/* Deal with non-option arguments here */
	int index;
	if ( optind < argc )
	{
		for (index = optind; index < argc; index++)
			if (fireThruster(argv[index], customPayload, language, compiler, hasMakefile, vcs) != EXIT_SUCCESS)
				return EXIT_FAILURE;
	}
	else
	{
		printf("No project, aborting launch.\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int fireThruster(char *name, char *customPayload, char *language, char *compiler, bool hasMakefile, char *vcs)
{
	printf("Launching %s:\n", name);

	lua_State *L = luaL_newstate();   /* opens Lua */
	luaL_openlibs(L);

	// checking for the path of the script and its payload
	FILE *file;
	if ((file = fopen("~/.config/ignition/thruster.lua", "r")))
	{
		fclose(file);
		luaL_loadfile(L, "~/.config/ignition/thruster.lua");
	}
	else
		luaL_loadfile(L, "/usr/share/ignition/thruster.lua");

	// PRIMING RUN
	if (lua_pcall(L,
				0, // number of arguments
				0, // number of returns
				0))
	{
		const char* error = lua_tostring(L, -1);
		lua_pop(L, 1);
		printf(error);
		return EXIT_FAILURE;
	}

	// TELL LUA WHAT FUNCTION TO RUN
	lua_getglobal(L, "liftoff");

	// TELL LUA WHAT THE ARGUMENTS ARE
	// pushing them to the stack
	lua_pushstring(L, name);
	lua_pushstring(L, customPayload);
	lua_pushstring(L, language);
	lua_pushstring(L, compiler);
	lua_pushboolean(L, hasMakefile);
	lua_pushstring(L, vcs);

	//ACTUAL RUN OF THE FUNCTION
	//RETURNS > 0
	if (lua_pcall(L,
				6, // number of arguments
				1, // number of returns
				0))
	{
		const char* error = lua_tostring(L, -1);
		lua_pop(L, 1);
		printf(error);
		return EXIT_FAILURE;
	}

	lua_close(L);
	return EXIT_SUCCESS;
}

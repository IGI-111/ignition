local rocket = {}
local lfs = require "lfs"

rocket.binPath = "bin" -- Compiled files will be stored here
rocket.srcPath = "src" -- Source files will be stored here
rocket.objPath = "obj" -- Object files will stored here

function rocket.payloadPath(payload)
	if(lfs.attributes(os.getenv("HOME").."/.config/ignition/payload/"..payload,"mode") == "directory") then --FIXME
		return os.getenv("HOME").."/.config/ignition/payload/"..payload
	elseif(lfs.attributes("/usr/share/ignition/payload/"..payload,"mode") == "directory") then
		return "/usr/share/ignition/payload/"..payload
	end
	return nil
end

function rocket.makefilePath(payload)
	if(lfs.attributes(os.getenv("HOME").."/.config/ignition/payload/Makefile/"..payload,"mode") == "directory") then --FIXME
		return os.getenv("HOME").."/.config/ignition/payload/Makefile/"..payload
	elseif(lfs.attributes("/usr/share/ignition/payload/Makefile/"..payload,"mode") == "directory") then
		return "/usr/share/ignition/payload/Makefile/"..payload
	end
	return nil
end

local function cp(source, dest)
	for filename in lfs.dir(source) do
		if filename ~= '.' and filename ~= '..' then
			local source_path = source .. '/' .. filename
			local attr = lfs.attributes(source_path)
			if type(attr) == "table" and attr.mode == "directory" then
				local dest_path = dest .. "/" .. filename
				print("Delivered " .. dest_path)
				lfs.mkdir(dest_path)
				deliver(source_path, dest_path)
			else
				local f = io.open(source_path, "rb")
				local content = f:read("*all")
				f:close()
				local w = io.open(dest .. "/" .. filename, "wb")
				print("Delivered " .. dest .. "/" .. filename)
				w:write(content)
				w:close()
			end
		end
	end
end

function rocket.deliverPayload(payload)
	if payload ~= nil and rocket.payloadPath(payload) ~= nil then
		cp(rocket.payloadPath(payload), rocket.srcPath)
	end
end


function rocket.deliverMakefile(name, language, compiler)
	if(language ~= nil and rocket.makefilePath(language) ~= nil) then
		cp(rocket.makefilePath(language), ".")

		makefile = io.open("Makefile", "r")
		buffer = makefile:read("*a")
		makefile:close()

		if name ~= nil then buffer = string.gsub(buffer,"TARGET%s*=%s*%S*","TARGET  = "..name) end
		if compiler ~= nil then
			buffer = string.gsub(buffer,"CC%s*=%s*%S*","CC      = "..compiler)
			buffer = string.gsub(buffer,"LINKER%s*=%s*%S*","LINKER  = "..compiler)
		end
		if rocket.srcPath ~= nil then buffer = string.gsub(buffer,"SRCPATH%s*=%s*%S*","SRCPATH = "..rocket.srcPath) end
		if rocket.objPath ~= nil then buffer = string.gsub(buffer,"OBJPATH%s*=%s*%S*","OBJPATH = "..rocket.objPath) end
		if rocket.binPath ~= nil then buffer = string.gsub(buffer,"BINPATH%s*=%s*%S*","BINPATH = "..rocket.binPath) end

		makefile = io.open("Makefile", "w+")
		makefile:write(buffer)
		makefile:close()
	end
end

function rocket.enableVersionControl(vcs)
	if vcs ~= nil then vcs = string.lower(vcs) end -- sets the vcs to lower to avoid case problems
	if vcs == "git" then
		os.execute("git init ".. rocket.srcPath)
	elseif vcs == "svn" then
		os.execute("svnadmin create ".. rocket.srcPath)
	elseif vcs == "mercurial" or vcs == "hg" then
		os.execute("hg init ".. rocket.srcPath)
	end
end

return rocket

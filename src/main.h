#ifndef _MAIN_HEADER_GUARD_
#define _MAIN_HEADER_GUARD_

int fireThruster(char *name, char *customPayload, char *language, char *compiler, bool hasMakefile, char *vcs);
#endif
